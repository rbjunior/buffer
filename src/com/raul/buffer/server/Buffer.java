package com.raul.buffer.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Buffer {

    private final ConcurrentLinkedQueue<Integer> buffer;
    private final int porta;
    private final int tamanhoBuffer;

    public Buffer(int porta, int tamanhoBuffer) {
        this.porta = porta;
        this.tamanhoBuffer = tamanhoBuffer;
        this.buffer = new ConcurrentLinkedQueue<>();
    }


    /**
     * Inicia um servidor e escuta por novas conex�es, disparando uma thread
     * para processar cada uma em separado
     * @throws IOException
     */
    public void rodarServidor() throws IOException {
        ServerSocket servidor = new ServerSocket(porta);
        ExecutorService service = Executors.newCachedThreadPool();

        while (true) {
            Socket cliente = servidor.accept();

            /**
             * Processa requisi��es em threads separadas
             */
            service.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
                        saida.flush();
                        ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());

                        HashMap<String, Object> mensagemEntrada = (HashMap) entrada.readObject();
                        HashMap<String, Object> mensagemSaida = processaMensagem(mensagemEntrada);
                        saida.writeObject(mensagemSaida);
                        saida.flush();
                    } catch (IOException | ClassNotFoundException ex) {
                        Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        try {
                            cliente.close();
                        } catch (IOException ex) {
                            Logger.getLogger(Buffer.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            });
        }
    }

    /**
     * Processa uma mensagem de entrada e produz uma mensagem de retorno
     * @param mensagemEntrada map com as informa��es da mensagem
     * @return map com a mensagem de retorno
     */
    private HashMap<String, Object> processaMensagem(HashMap mensagemEntrada) {
        HashMap<String, Object> mensagemSaida = new HashMap();
        boolean sucesso = false;
        if (mensagemEntrada.containsKey("operacao")) {
            if ("+".equals(mensagemEntrada.get("operacao")) && mensagemEntrada.containsKey("item")) {
                if (!adicionaItem((Integer) mensagemEntrada.get("item"), (String) mensagemEntrada.get("quem"))) {
                    mensagemSaida.put("codigo", "BUFFER_CHEIO");
                } else {
                    sucesso = true;
                }
            } else if ("-".equals(mensagemEntrada.get("operacao"))) {
                Integer item = removeItem((String) mensagemEntrada.get("quem"));
                if (null == item) {
                    mensagemSaida.put("codigo", "BUFFER_VAZIO");
                } else {
                    mensagemSaida.put("item", item);
                    sucesso = true;
                }
            }
        }

        mensagemSaida.put("status", sucesso);
        return mensagemSaida;
    }

    /**
     * Adiciona um novo item ao buffer, caso ele n�o esteja cheio
     * @param item n�mero
     * @param quem Nome do cliente
     * @return boolean sucesso
     */
    private synchronized boolean adicionaItem(Integer item, String quem) {
        if (buffer.size() == tamanhoBuffer) {
            return false;
        } else {
            buffer.add(item);
            System.out.println("Valor " + item + " adicionado em Buffer pelo " + quem);
        }

        return true;
    }

    /**
     * Remove um item do buffer, caso ele n�o esteja vazio
     * @param quem Nome do cliente
     * @return item retirado
     */
    private synchronized Integer removeItem(String quem) {
        Integer item = buffer.poll();
        if (item != null) {
            System.out.println("Valor " + item + " retirado em Buffer pelo " + quem);
        }
        return item;
    }
}
