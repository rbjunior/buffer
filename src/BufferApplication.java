import com.raul.buffer.server.Buffer;
import java.io.IOException;

/**
 * Aplicação responsável por rodar o buffer
 */
public class BufferApplication {

    public static void main(String[] args) throws IOException {
        if (args.length < 2) {
            System.out.println("Parâmetros inválidos");
            return;
        }
        int porta;
        int tamanhoPilha;
        try {
            porta = Integer.parseInt(args[0]);
            tamanhoPilha = Integer.parseInt(args[1]);
        } catch (NumberFormatException ex) {
            System.out.println("Parâmetros inválidos");
            return;
        }

        Buffer buffer = new Buffer(porta, tamanhoPilha);

        buffer.rodarServidor();
    }
}
